<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Forms;

use Symfony\Component\Form\AbstractType,
    Symfony\Component\Form\FormBuilderInterface,
    Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RegistrationType
 *
 */
class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name', 'text', [
            'label'=>'First name',
            'attr' => [
                'placeholder'   => 'First name',
                'class'         => 'form-control'
            ]
        ])
        ->add('last_name', 'text', [
            'label'=>'Last name',
            'attr' => [
                'placeholder'   => 'Last name',
                'class'         => 'form-control'
            ]
        ])
        ->add('password', 'password',[
            'label'=>'Password',
            'attr' => [ 
                'placeholder' => 'Password', 
                'class' => 'form-control'
            ]
        ])
        ->add('confirm', 'password', [ 
            'mapped' => false,
            'label'=>'Re-type password', 
            'attr' => [
                'placeholder' => 'Repeat password',
                'class' => 'form-control'
            ]
        ])
        ->add('email', 'email', [
            'label' => 'Email',
            'attr' => [
                'placeholder' => 'Email',
                'class' => 'form-control'
            ]
        ])
        ->add('save', 'submit', [
            'label'=>'Register', 
            'attr' => [
                'class' => 'form-control'
            ]
        ]);
    }

    public function getName()
    {
        return 'registration';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Model\Entity\User',
        ]);
    }
}
