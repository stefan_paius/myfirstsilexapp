<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\RedirectResponse,
    Symfony\Component\Routing\Generator\UrlGenerator,
    Symfony\Component\Security\Core\SecurityContext,
    Symfony\Component\HttpFoundation\Request,
    Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder,
    Symfony\Bundle\FrameworkBundle\Controller\Controller,
    App\TestInjection\TestInterface,
    App\Model\Entity\User,
    App\Forms\RegistrationType,
    Twig_Environment;

class HomeController extends Controller {
    
    public function __construct($twig) {
        $this->twig = $twig;
    }

    public function indexAction(Twig_Environment $twig, TestInterface $t) {
        $encoder = new MessageDigestPasswordEncoder();
        echo $encoder->encodePassword('FakePass.02', '') . '</br>';
        echo $t->writeMessage();
        return $twig->render('index.html.twig', array());
    }

    public function registerAction(\App\Application $app, Request $request) {
        $user = new User();

        $form = $app['form.factory']
            ->create(new RegistrationType( ), $user);
        
        //todo handle submit
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            return $this->redirectToRoute('replace_with_some_route');
        }
            
        return $this->twig->render('register.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function loginAction(Request $req, Twig_Environment $twig, SecurityContext $sc, UrlGenerator $urlgen) {
        if ($sc->isGranted('IS_AUTHENTICATED_FULLY')) {
            return new RedirectResponse($urlgen->generate('home'));
        } else {
            $session = $req->getSession();
            $errorConst = $sc::AUTHENTICATION_ERROR;
            $lastUsernameConst = $sc::LAST_USERNAME;

            return $twig->render('login.html.twig', array(
                        'error' => ($session->has($errorConst)) ? $session->get($errorConst)->getMessage() : null,
                        'last_username' => $session->get($lastUsernameConst),
            ));
        }
    }

}
